﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Certificetes;
using System.Security.Cryptography.X509Certificates;
using System.Xml;
using XmlSigner;
using System.Security.Cryptography;
using System.IO;

namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            X509Certificate2 cert =  T_Certificates();
            T_XmlSigner(cert);
            T_VerifyXml();
            Console.WriteLine("Press enter to close...");
            Console.ReadLine();

        }

        #region Tests
        private static X509Certificate2 T_Certificates()
        {
            string serialNumber = "55416cf49e0139a1";
            //return  Certificate.CreateCertificate2("Marko Alex");
            // Certificate.InstallCertificate(StoreLocation.CurrentUser);
           return Certificate.GetCertificateFromStore(StoreLocation.CurrentUser, serialNumber);
            //Certificate.UninstallCertificate(StoreLocation.CurrentUser, serialNumber);
            //var certificate = Certificate.GetCertificateFromStore(StoreLocation.CurrentUser, serialNumber);
        }

        private static void T_XmlSigner(X509Certificate2 cert)
        {
            string serialNumber = "55416cf49e0139a1";
            string path = "d:\\Projects\\TestXml\\NotSignedXml\\Test.xml";
            string signedPath = @"d:\Projects\TestXml\SignedXml\SignedTest.xml";
            XmlDocument doc = new XmlDocument();
            doc.Load(path);
            //var certificate = Certificate.GetCertificateFromStore(StoreLocation.CurrentUser, serialNumber);
            if(cert != null)
            {
                 // Generate a signing key.
                RSACryptoServiceProvider Key = new RSACryptoServiceProvider();
                //string signedXml = Signer.SignXml_4(path, Key);
                 //string signedXml = Signer.SignXml(doc.OuterXml, certificate);
                var signedXml = Signer.SignXml(doc.OuterXml, cert);
                using (FileStream stream = File.Create(signedPath))
                {
                    byte[] info = new UTF8Encoding(true).GetBytes(signedXml.OuterXml);
                    stream.Write(info, 0, info.Length);

                }
            }
        }

        private static void T_VerifyXml()
        {
            string signedPath = @"d:\Projects\TestXml\SignedXml\SignedTest.xml";
            string dataFromFile = File.ReadAllText(signedPath);
            if(dataFromFile.VerifyXml())
            {
                Console.WriteLine("==================\nXml is valid");
            }
            else
            {
                Console.WriteLine("==================\nXml is NOT valid");
            }
        }

        
        #endregion
    }
}
