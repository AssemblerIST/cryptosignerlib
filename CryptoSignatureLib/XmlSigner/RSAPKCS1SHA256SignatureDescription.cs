﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace XmlSigner
{
     public class RsaPkcs1Sha256SignatureDescription : SignatureDescription
     {
         // SHA256 URL Identifiers
         public const string XmlDsigMoreRsaSha256Url = @"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"; // Signature algoritm
         public const string XmlEncSha256Url = @"http://www.w3.org/2001/04/xmlenc#sha256"; // Digest algorithm

         /// <summary>
         /// Registers the http://www.w3.org/2001/04/xmldsig-more#rsa-sha256 algorithm
         /// with the .NET CrytoConfig registry. This needs to be called once per
         /// appdomain before attempting to validate SHA256 signatures.
         /// </summary>
         public static void Register()
         {
             CryptoConfig.AddAlgorithm(
                 typeof(RsaPkcs1Sha256SignatureDescription), XmlDsigMoreRsaSha256Url);
         }

         /// <summary>
         /// .NET calls this parameterless ctor
         /// </summary>
         public RsaPkcs1Sha256SignatureDescription()
         {
             KeyAlgorithm = "System.Security.Cryptography.RSACryptoServiceProvider";
             DigestAlgorithm = "System.Security.Cryptography.SHA256Managed";
             FormatterAlgorithm = "System.Security.Cryptography.RSAPKCS1SignatureFormatter";
             DeformatterAlgorithm = "System.Security.Cryptography.RSAPKCS1SignatureDeformatter";
         }

         public override AsymmetricSignatureDeformatter CreateDeformatter(AsymmetricAlgorithm key)
         {
             var asymmetricSignatureDeformatter =
                 (AsymmetricSignatureDeformatter)CryptoConfig.CreateFromName(DeformatterAlgorithm);
             asymmetricSignatureDeformatter.SetKey(key);
             asymmetricSignatureDeformatter.SetHashAlgorithm("SHA256");
             return asymmetricSignatureDeformatter;
         }

         public override AsymmetricSignatureFormatter CreateFormatter(AsymmetricAlgorithm key)
         {
             var asymmetricSignatureFormatter =
                 (AsymmetricSignatureFormatter)CryptoConfig.CreateFromName(FormatterAlgorithm);
             asymmetricSignatureFormatter.SetKey(key);
             asymmetricSignatureFormatter.SetHashAlgorithm("SHA256");
             return asymmetricSignatureFormatter;
         }
     }
    /*public sealed class RSAPKCS1SHA256SignatureDescription : SignatureDescription
    {
        /// <summary>Initializes a new instance of the <see cref="T:System.Deployment.Internal.CodeSigning.RSAPKCS1SHA256SignatureDescription" /> class.</summary>
        public RSAPKCS1SHA256SignatureDescription()
        {
            base.KeyAlgorithm = typeof(RSACryptoServiceProvider).FullName;
            base.DigestAlgorithm = typeof(SHA256).FullName;
            base.FormatterAlgorithm = typeof(RSAPKCS1SignatureFormatter).FullName;
            base.DeformatterAlgorithm = typeof(RSAPKCS1SignatureDeformatter).FullName;
        }

        /// <summary>Creates an asymmetric signature deformatter instance that has the specified key.</summary>
        /// <param name="key">The key to use in the deformatter. </param>
        /// <returns>An asymmetric signature deformatter object.</returns>
        public override AsymmetricSignatureDeformatter CreateDeformatter(AsymmetricAlgorithm key)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }
            RSAPKCS1SignatureDeformatter rSAPKCS1SignatureDeformatter = new RSAPKCS1SignatureDeformatter(key);
            rSAPKCS1SignatureDeformatter.SetHashAlgorithm("SHA256");
            return rSAPKCS1SignatureDeformatter;
        }

        /// <summary>Creates an asymmetric signature formatter instance that has the specified key.</summary>
        /// <param name="key">The key to use in the formatter. </param>
        /// <returns>An asymmetric signature formatter object.</returns>
        public override AsymmetricSignatureFormatter CreateFormatter(AsymmetricAlgorithm key)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }
            RSAPKCS1SignatureFormatter rSAPKCS1SignatureFormatter = new RSAPKCS1SignatureFormatter(key);
            rSAPKCS1SignatureFormatter.SetHashAlgorithm("SHA256");
            return rSAPKCS1SignatureFormatter;
        }
    }*/
}
