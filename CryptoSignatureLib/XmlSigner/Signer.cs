﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Deployment.Internal.CodeSigning;
namespace XmlSigner
{
    public static class Signer
    {
        public const string SignatureMethod = "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256";

        public const string DigestMethod = "http://www.w3.org/2001/04/xmlenc#sha256";

        #region Interfaces  
            public static XmlDocument SignXml(this string xml, X509Certificate2 cert)
            {
                if(xml.IsXml())
                {
                    try
                    {
                       // CryptoConfig.AddAlgorithm(typeof(RSAPKCS1SHA256SignatureDescription), "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
                        RsaPkcs1Sha256SignatureDescription.Register();

                        var cspParams = new CspParameters(24) { KeyContainerName = "XML_DSIG_RSA_KEY" };
                        var key = new RSACryptoServiceProvider(cspParams);
                        key.FromXmlString(cert.PrivateKey.ToXmlString(true));

                        XmlDocument document = new XmlDocument();
                        document.LoadXml(xml);
                        AsymmetricAlgorithm privateKey = key;
                       // try
                       // {
                       //     privateKey = cert.PrivateKey;
                       // }
                       // catch
                       // {
                            privateKey = cert.GetRSAPrivateKey();
                       // }


                        SignedXml signedXml = new SignedXml(document);
                        signedXml.SigningKey = /*key*/privateKey;
                        signedXml.SignedInfo.SignatureMethod = SignatureMethod; 

                        Reference reference = new Reference();
                        reference.Uri = string.Empty;

                        Transform env = new XmlDsigEnvelopedSignatureTransform();
                        reference.AddTransform(env);

                        Transform C14T = new XmlDsigExcC14NTransform();
                        reference.AddTransform(C14T);
                        reference.DigestMethod = DigestMethod;

                        signedXml.AddReference(reference);

                        KeyInfo keyInfo = new KeyInfo();
                        keyInfo.AddClause(new RSAKeyValue((RSA)privateKey));

                        KeyInfoX509Data keyInfoData = new KeyInfoX509Data(cert);
                        keyInfo.AddClause(keyInfoData);

                        KeyInfoName kin = new KeyInfoName(cert.SubjectName.Name);
                        keyInfo.AddClause(kin);

                        signedXml.KeyInfo = keyInfo;

                        signedXml.ComputeSignature();

                        XmlElement xmlDigitalSignature = signedXml.GetXml();

                        document.DocumentElement.AppendChild(document.ImportNode(xmlDigitalSignature, true));

                        return document;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        throw;
                    }
                
                }
                return null;
            }
        
        public static bool VerifyXml(this string xmlString )
        {
            try
            {

                int countSignatures = 1;
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(xmlString);
                SignedXml signedXml = new SignedXml(xmlDocument);
                XmlNodeList nodeListSignature = xmlDocument.GetElementsByTagName("Signature");
                if (nodeListSignature.Count < 1)
                {
                    throw new Exception("В файле нет узлов с названием Signature");
                }    

                foreach (XmlNode nodeSignature in nodeListSignature )
                {
                    var nodeX509Certificate = xmlDocument.GetElementsByTagName("X509Certificate");
                    if(nodeX509Certificate.Count < 1)
                    {
                        throw new Exception("В файле нет узлов с названием X509Certificate");
                    }
                    X509Certificate2 certificate = new X509Certificate2();

                    byte[] rawData = Encoding.UTF8.GetBytes(nodeX509Certificate[0].InnerText);
                    certificate.Import(rawData);

                    signedXml.LoadXml((XmlElement)nodeSignature);
                    if (signedXml.CheckSignature(certificate, true))
                    {
                        WriteLine($"Подпись № {countSignatures} является валидной", ConsoleColor.Green);
                    }
                    else
                    {
                        WriteLine($"Подпись № {countSignatures } является не валидной",ConsoleColor.Red);
                    }
                    countSignatures += 1;
                }
            }
            catch(Exception e)
            {
                WriteLine(e.Message, ConsoleColor.Red);
                return false;
            }

            return true;
        }
        #endregion

        /// <summary>
        /// проверка, если документ xml
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        private static bool IsXml(this string xml)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xml);
                return true;
            }
            catch(Exception e)
            {
                WriteLine(e.Message, ConsoleColor.Red);
                return false;
            }
        }

        /// <summary>
        /// Цветной вывод в консоль
        /// </summary>
        /// <param name="message"></param>
        /// <param name="color"></param>
        private static void WriteLine(string message, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
