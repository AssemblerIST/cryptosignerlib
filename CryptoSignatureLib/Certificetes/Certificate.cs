﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

using Org.BouncyCastle.X509;
using System.Text;
using System.Threading.Tasks;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Utilities;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Operators;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.Crypto.Prng;


/*
 *TODO:
 * - реализовать класс для подписи xml
 * - реализовать проверку валидности подписаннорй xml
 */
namespace Certificetes
{


    public static class Certificate
    {
        /// <summary>
        /// Путь к созданному сертификату
        /// </summary>
        private static string _fileNameCert;

        #region Interfaces
        /// <summary>
        /// Создание сертификата
        /// </summary>
        public static void CreateCertificate()
        {
            var ecdsa = ECDsa.Create(); // generate asymmetric key pair
            var req = new CertificateRequest("C=PMR, O=AssmCorps, CN=Marko Alexandr", ecdsa, HashAlgorithmName.SHA256);
            var cert = req.CreateSelfSigned(DateTimeOffset.Now, DateTimeOffset.Now.AddYears(5));

            string localPathCert = System.IO.Path.GetDirectoryName(new System.Uri
                (System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath);
            _fileNameCert = $"{localPathCert}\\TestCertificates\\AssmCert.cer";
            // Create PFX (PKCS #12) with private key
            File.WriteAllBytes($"{localPathCert}\\TestCertificates\\AssmCert.pfx", cert.Export(X509ContentType.Pfx, "P@55w0rd"));

            // Create Base 64 encoded CER (public key only)
            File.WriteAllText(_fileNameCert,
                "-----BEGIN CERTIFICATE-----\r\n"
                + Convert.ToBase64String(cert.Export(X509ContentType.Cert), Base64FormattingOptions.InsertLineBreaks)
                + "\r\n-----END CERTIFICATE-----");
        }

        public static X509Certificate2 CreateCertificate2(string subject)
        {
            var random = new SecureRandom();
            var certificateGenerator = new X509V3CertificateGenerator();

            var serialNumber = BigIntegers.CreateRandomInRange(BigInteger.One, BigInteger.ValueOf(Int64.MaxValue), random);
            certificateGenerator.SetSerialNumber(serialNumber);

            certificateGenerator.SetIssuerDN(new X509Name($"C=PMR, O=AssmCorps, CN={subject}"));
            certificateGenerator.SetSubjectDN(new X509Name($"C=PMR, O=AssmCorps, CN={subject}"));
            certificateGenerator.SetNotBefore(DateTime.UtcNow.Date);
            certificateGenerator.SetNotAfter(DateTime.UtcNow.Date.AddYears(1));

            const int strength = 2048;
            var keyGenerationParameters = new KeyGenerationParameters(random, strength);
            var keyPairGenerator = new RsaKeyPairGenerator();
            keyPairGenerator.Init(keyGenerationParameters);

            var subjectKeyPair = keyPairGenerator.GenerateKeyPair();
            certificateGenerator.SetPublicKey(subjectKeyPair.Public);

            var issuerKeyPair = subjectKeyPair;
            const string signatureAlgorithm = "SHA256WithRSA";
            var signatureFactory = new Asn1SignatureFactory(signatureAlgorithm, issuerKeyPair.Private);
            var bouncyCert = certificateGenerator.Generate(signatureFactory);
            //certificateGenerator.SetSignatureAlgorithm(signatureAlgorithm);
            // Lets convert it to X509Certificate2
            X509Certificate2 certificate;

            Pkcs12Store store = new Pkcs12StoreBuilder().Build();
            store.SetKeyEntry($"{subject}_key", new AsymmetricKeyEntry(subjectKeyPair.Private), new[] { new X509CertificateEntry(bouncyCert) });
            string exportpw = Guid.NewGuid().ToString("x");

            using (var ms = new System.IO.MemoryStream())
            {
                store.Save(ms, exportpw.ToCharArray(), random);
                certificate = new X509Certificate2(ms.ToArray(), exportpw, X509KeyStorageFlags.Exportable);
            }

            //Console.WriteLine($"Generated cert with thumbprint {certificate.Thumbprint}");
            /* string localPathCert = System.IO.Path.GetDirectoryName(new System.Uri
                   (System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath);
             _fileNameCert = $"{localPathCert}\\TestCertificates\\AssmCert2.cer";
             File.WriteAllText(_fileNameCert,
              "-----BEGIN CERTIFICATE-----\r\n"
              + Convert.ToBase64String(certificate.Export(X509ContentType.Cert), Base64FormattingOptions.InsertLineBreaks)
              + "\r\n-----END CERTIFICATE-----");*/

            byte[] certData = certificate.Export(X509ContentType.Pfx);
            File.WriteAllBytes(@"D:\Projects\TestCertificates\NewAssm.pfx", certData);
            return certificate;
        }

        /// <summary>
        /// Установка сертификать в store
        /// </summary>
        public static void InstallCertificate(StoreLocation typeStore)
        {
            try
            {
                X509Certificate2 certificate = new X509Certificate2();
                byte[] rawData = ReadFile();
                certificate.Import(rawData);
                using (X509Store store = new X509Store(StoreName.My, typeStore))
                {
                    store.Open(OpenFlags.ReadWrite);
                    store.Add(certificate);
                    store.Close();
                }
            }
            catch (Exception e)
            {
                WriteLine(e.Message, ConsoleColor.Red);
            }
        }

        /// <summary>
        /// Удалить сертификат из store
        /// </summary>
        public static void UninstallCertificate(StoreLocation typeStore, string serialNumber)
        {
            try
            {
                using (X509Store store = new X509Store(StoreName.My, typeStore))
                {
                    store.Open(OpenFlags.ReadWrite | OpenFlags.IncludeArchived);        
                    X509Certificate2Collection col = store.Certificates.Find(X509FindType.FindBySerialNumber, serialNumber, false);
                    foreach (var cert in col)
                    {
                        store.Remove(cert);
                        WriteLine($"Certificate with serial number {serialNumber} was removed", ConsoleColor.Green);
                    }
                }
            }
            catch (Exception e)
            {
                WriteLine(e.Message, ConsoleColor.Red);
            }            
        }

        /// <summary>
        /// Получает сертификат из store
        /// </summary>
        public static X509Certificate2 GetCertificateFromStore(StoreLocation typeStore, string serialNumber)
        {
            X509Certificate2 certificate = null;
            try
            {
                using (X509Store store = new X509Store(typeStore))
                {
                    store.Open(OpenFlags.ReadOnly);
                    X509Certificate2Collection cers = store.Certificates.Find(X509FindType.FindBySerialNumber, serialNumber, false);
                    if (cers.Count > 0)
                    {
                        certificate = cers[0];
                    }
                    else
                    {
                        throw new Exception($"Certificate not exist with current serial number = {serialNumber}");
                    }             
                }
            }
            catch(Exception e)
            {
                WriteLine(e.Message, ConsoleColor.Red);
                certificate = null;
            }
            return certificate;
        }
        #endregion

        /// <summary>
        /// Считывает данный из сертификата локально
        /// </summary>
        /// <returns></returns>
        private static byte[] ReadFile()
        {

            using (FileStream f = new FileStream(_fileNameCert, FileMode.Open, FileAccess.Read))
            {
                try
                {
                    int size = (int)f.Length;
                    byte[] data = new byte[size];
                    size = f.Read(data, 0, size);
                    f.Close();
                    return data;
                }
                catch(Exception e)
                {
                    WriteLine(e.Message, ConsoleColor.Red);
                    throw;
                }

            }
        }

        /// <summary>
        /// Цветной вывод в консоль
        /// </summary>
        /// <param name="message"></param>
        /// <param name="color"></param>
        private static void WriteLine(string message, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
